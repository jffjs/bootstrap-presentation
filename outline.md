!SLIDE title-and-content transition=cover incremental

# Outline

- What is Bootstrap?
- What Bootstrap can do
- Bootstrap and Rails
- An example: Vonage Mobile Prototype
- Additional Resources