!SLIDE title-and-content transition=cover incremental

# Bootstrap Javascripts

* Includes several easy to use Javascripts:
[documentation](http://getbootstrap.com/javascript/)

* Modals, dropdowns, accordians, and more

* Can be used purely in an unobtrusive way without writing a single line
of javascript:

    ```html
      <button type="button" data-toggle="modal" data-target="#myModal">Launch modal</button>
    ```
