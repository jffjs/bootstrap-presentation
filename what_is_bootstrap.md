!SLIDE title-and-content transition=cover incremental

# What is Bootstrap?

- A web application framework implementing <span style="color:#f5582F">responsive design</span>
- Bootstrap consists of:
  - CSS styles that
      - provide a grid based layout based on CSS3 @media tag
      - act as components
      - add styling
  - Javascript
      - for components with behavior 
- Bootstrap version 3 is the most current release
  - <http://getbootstrap.com/> 
  

!SLIDE title-and-content transition=cover incremental

# Responsive Design

- A single design and codebase that adapts to any device or screen size, automatically.
- Older applications use different subdomains for desktop and mobile sites:

  |||
  |---|---|
  | desktop | www.washingtonpost.com |
  |  mobile |  m.washingtonpost.com |

  - A web server serves different pages to different devices based on the user-agent in the http request
- Newer applications send the same page to each device and use the CSS3 @media tag to make the page responsive


!SLIDE title-and-content transition=cover incremental

# The CSS3 @media tag

```css
@media (max-width: 600px) {
  ...
}

@media (max-width: 400px) {
 ...
}

@media (min-width: 1300px) {
  ...
}
```

- [Here is an example](http://alistapart.com/d/responsive-web-design/ex/ex-site-FINAL.html) 
  - <http://alistapart.com/d/responsive-web-design/ex/ex-site-FINAL.html>
