!SLIDE title-and-content transition=cover

# Bootstrap and Rails

## Install from source

* Prerequisites: [Node.js](http://nodejs.org), [Grunt](http://gruntjs.com/)

    ```bash
      brew install node  # install node.js
      sudo npm install -g grunt-cli # install Grunt globally
    ```


!SLIDE title-and-content transition=cover incremental

# Bootstrap and Rails

## Install from source

* Grab the source and install
 
    ```bash
      git clone https://github.com/twbs/bootstrap.git
      cd bootstrap
      npm install # installs required node.js modules locally
    ```

* Edit less files to your hearts content
 
* Compile
 
    ```bash
      grunt dist  # compiles less files, concats, and minifies
    ```
