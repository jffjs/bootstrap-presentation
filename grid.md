!SLIDE title-and-content transition=cover

# Bootstrap CSS

![The Gride](images/the-grid.jpg)


!SLIDE title-and-content transition=cover

# Bootstrap CSS

## The Grid

* Each row has 12 columns

    ```html
    <div class="row">
      <div class="col-md-8">.col-md-8</div>
      <div class="col-md-4">.col-md-4</div>
    </div>
    ```

![columns](images/columns.png)


!SLIDE title-and-content transition=cover

# Bootstrap CSS

## The Grid

* Columns can be nested

    ```html
    <div class="row">
      <div class="col-md-9">
        Level 1: .col-md-9
        <div class="row">
          <div class="col-md-6">
            Level 2: .col-md-6
          </div>
          <div class="col-md-6">
            Level 2: .col-md-6
          </div>
        </div>
      </div>
    </div>
    ```

![columns nested](images/columns-nested.png)


!SLIDE title-and-content transition=cover

# Bootstrap CSS

## The Grid

* Grid is designed with responsiveness in mind

![responsive grid](images/responsive-grid.png)


!SLIDE title-and-content transition=cover

# Bootstrap CSS

## The Grid

* Design for mobile, tablet, and desktop all at once

    ```html
    <div class="row">
      <div class="col-xs-12 col-sm-6 col-md-8">.col-xs-12 .col-sm-6 .col-md-8</div>
      <div class="col-xs-6 col-sm-6 col-md-4">.col-xs-6 .col-sm-6 .col-md-4</div>
    </div>
    <div class="row">
      <div class="col-xs-6 col-sm-4 col-md-4">.col-xs-6 .col-sm-4 .col-md-4</div>
      <div class="col-xs-6 col-sm-4 col-md-4">.col-xs-6 .col-sm-4 .col-md-4</div>
      <!-- Optional: clear the XS cols if their content doesn't match in height -->
      <div class="clearfix visible-xs"></div>
      <div class="col-xs-6 col-sm-4 col-md-4">.col-xs-6 .col-sm-4 .col-md-4</div>
    </div>
    ```

!SLIDE title-and-content transition=cover

# Bootstrap CSS

## The Grid

* On desktop
 
![responsive medium](images/responsive-md.png)


!SLIDE title-and-content transition=cover

# Bootstrap CSS

## The Grid

* On tablet
 
![responsive small](images/responsive-sm.png)


!SLIDE title-and-content transition=cover

# Bootstrap CSS

## The Grid

* On phone
 
![responsive small](images/responsive-xs.png)

