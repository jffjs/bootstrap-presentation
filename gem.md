!SLIDE title-and-content transition=cover incremental

# Bootstrap and Rails

## [twitter-bootstrap-rails](https://github.com/seyhunak/twitter-bootstrap-rails) gem

* Integrates Bootstrap into Rails using Asset Pipeline

* Provides generators and view helpers that generate Bootstrap styled
html

* Generate layout
 
    ```
      rails g bootstrap:layout [LAYOUT_NAME]
    ```

* Include only the parts you want

    ```less
      @import "twitter/bootstrap/grid.less";
      @import "twitter/bootstrap/forms.less";
    ```
