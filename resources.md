!SLIDE title-and-content transition=cover incremental

# Resources
- Bootstrap Documentation
  - <http://getbootstrap.com/>
- Bootstrap Source
  - <https://github.com/twbs/bootstrap/>
- Ryan Bates's Railscasts
  - [#328 Twitter Bootstrap Basics](http://railscasts.com/episodes/328-twitter-bootstrap-basics?autoplay=true)
  - [#329 More on Twitter Bootstrap](http://railscasts.com/episodes/329-more-on-twitter-bootstrap)
- bootstrap-rails gem
  - <https://github.com/seyhunak/twitter-bootstrap-rails>
 

!SLIDE title-and-content transition=cover

# Resources
* This presentation
 
    ```bash
      git clone https://jffjs@bitbucket.org/jffjs/bootstrap-presentation.git
    ```
