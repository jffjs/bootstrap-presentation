!SLIDE title-and-content transition=cover

# Bootstrap Documents

* Online: www.getbootstrap.com
 
![getbootstrap.com](images/getbootstrap-com.png) 


!SLIDE title-and-content incremental transition=cover
# Bootstrap Documents

## Run locally with [Jekyll](http://www.jekyllrb.com)
 
* Grab bootstrap source
 
    ```bash
        git clone https://github.com/twbs/bootstrap.git
        cd bootstrap
    ```

* Install Jekyll
 
    ```bash
      gem install jekyll
    ```

* Run Jekyll from bootstrap directory

    ```bash
      jekyll serve
    ```

* Navigate to [localhost:9001]( http://localhost:9090 )
