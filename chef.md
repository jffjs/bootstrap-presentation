!SLIDE content transition=cover

# Provisioning

>

![provisioning] (http://localhost:9090/image/images/provisioning.jpg)



!SLIDE title-and-content incremental transition=cover

# Use Cases

* Provide **clean clone of production environment**. You can guarantee that that your staging server is a exact clone of
your production server.

* **Team of developers working along the globe and on different platforms**. The idea is to do development against
unified, easy to replicate virtual platform with same characteristics for all developers.

* **Configure new workstation for new employee**, e.g. macbook configuration (rvm, ruby, mysql, postgesql, skype, iterm2 etc.)



!SLIDE title-and-content incremental transition=zoom

# Avaliable options for provisioning

* Create simple shell script
* Use chef gem
* Use puppet gem (out of scope)
* Provisioning from Github: [Boxen](http://boxen.github.com/) - tool for automating and managing Macs with Puppet

