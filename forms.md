!SLIDE title-and-content transition=cover

# Bootstrap CSS

## Forms

* Great looking forms built-in: [documentation](http://getbootstrap.com/css/#forms)

* Default form:

![default form](images/form-default.png)


!SLIDE title-and-content transition=cover

# Bootstrap CSS

## Forms

* Horizontal form:

![default form](images/form-horizontal.png)


!SLIDE title-and-content transition=cover

# Bootstrap CSS

## Forms

* Inline form:

![default form](images/form-inline.png)
