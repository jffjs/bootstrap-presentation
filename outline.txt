1. what is bootstrap
  - responsive design
  - CSS3 @media tag
  - architecture
    - components
    - styles
  
2. sampler (doc with jeckyl)
  - css\
    - grid
    - built in classes for styles
    
  - javascript
    - unobtrusive javascript
  
3. rails integration
  - using less
  - using asset pipeline

4. example
  - vonage prototype
    - haml
    - grid
    - css

5. additional resources
  - railscasts
  - github
    - less
    - bootstrap rails
  - etc
